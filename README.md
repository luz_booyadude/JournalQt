# Simple Journal in Qt

Build Status: [![build status](https://gitlab.com/luz_booyadude/JournalQt/badges/master/build.svg)](https://gitlab.com/luz_booyadude/JournalQt/commits/master)

## Instruction
1. `CTRL + s` to save.
2. `CTRL + =` to increase font.
3. `CTRL + -` to decrease font.