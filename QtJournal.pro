#-------------------------------------------------
#
# Project created by QtCreator 2017-08-05T15:18:42
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtJournal
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sqlitemanager.cpp

HEADERS  += mainwindow.h \
    sqlitemanager.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc

RC_ICONS = img\main.ico
