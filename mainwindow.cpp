#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include "sqlitemanager.h"

QString MainWindow::VERSION = "v0.6";
bool MainWindow::UNSAVED;
bool MainWindow::BUTTON_ISCLICKED;
const QString MainWindow::WATERMARK = " - Journal by cr1tikal";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->plainTextEdit->installEventFilter(this);  // install event filter for keypress

    ui->label->setText(VERSION);  // set version number to label

    SqliteManager db;
    UNSAVED = false;
    BUTTON_ISCLICKED = false;

    QString date = QDate::currentDate().toString("yyyy-MM-dd");//ui->dateEdit->text();
    int day = QDate::currentDate().dayOfWeek();
    QString weekday = QDate::longDayName(day);
    this->setWindowTitle(date + " " + weekday + WATERMARK);
    ui->dateEdit->setDisplayFormat("yyyy-MM-dd");  // change date display format
    ui->dateEdit->setDate(QDate::currentDate());  // set current date
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SaveNotes()
{
    if(UNSAVED)
    {
        QString date = this->ui->dateEdit->date().toString("yyyy-MM-dd");
        QString content = this->ui->plainTextEdit->toPlainText().replace(QString("'"), QString("''"));
        if(SqliteManager::Query("REPLACE INTO `journal` (date, content) values ('" + date + "', '" + content + "')") == -1)
        {
            SqliteManager::messageBoxShow("Error writing to database!");
        }
        UNSAVED = false;
        this->setWindowTitle(date + WATERMARK);
    }
}

void MainWindow::on_btnToday_clicked()
{
    BUTTON_ISCLICKED = true;
    SaveNotes();
    QDate date = QDate::currentDate();
    int day = QDate::currentDate().dayOfWeek();
    QString weekday = QDate::longDayName(day);
    ui->dateEdit->setDate(date);
    this->setWindowTitle(date.toString("yyyy-MM-dd") + " " + weekday + WATERMARK);
}

void MainWindow::on_btnPrevious_clicked()
{
    BUTTON_ISCLICKED = true;
    SaveNotes();
    QDate date = ui->dateEdit->date();
    date = date.addDays(-1);
    int day = date.dayOfWeek();
    QString weekday = QDate::longDayName(day);
    ui->dateEdit->setDate(date);
    this->setWindowTitle(date.toString("yyyy-MM-dd") + " " + weekday + WATERMARK);
}

void MainWindow::on_btnNext_clicked()
{
    BUTTON_ISCLICKED = true;
    SaveNotes();
    QDate date = ui->dateEdit->date();
    date = date.addDays(1);
    int day = date.dayOfWeek();
    QString weekday = QDate::longDayName(day);
    ui->dateEdit->setDate(date);
    this->setWindowTitle(date.toString("yyyy-MM-dd") + " " + weekday + WATERMARK);
}

void MainWindow::on_dateEdit_dateChanged(const QDate &date)
{
    ui->plainTextEdit->clear();
    QSqlQuery query = SqliteManager::QueryScalar("select content from journal where date='" + date.toString("yyyy-MM-dd") + "'");
    QString note = "";
    if(query.first())
        note = query.value(0).toString();

    ui->plainTextEdit->appendPlainText(note);
    if(!BUTTON_ISCLICKED)
    {
        int day = date.dayOfWeek();
        QString weekday = QDate::longDayName(day);
        this->setWindowTitle(date.toString("yyyy-MM-dd") + " " + weekday + WATERMARK);
    }
    BUTTON_ISCLICKED =  false;
}

bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    if(object == ui->plainTextEdit && event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        /* keyboard shortcut for saving the notes */
        if(QApplication::keyboardModifiers() && Qt::ControlModifier && (keyEvent->key() == Qt::Key_S))
        {
            SaveNotes();
            QDate date = ui->dateEdit->date();
            int day = date.dayOfWeek();
            QString weekday = QDate::longDayName(day);
            this->setWindowTitle(date.toString("yyyy-MM-dd") + " " + weekday + WATERMARK);
            return QMainWindow::eventFilter(object, event);
        }
        /* keyboard shortcut for next days*/
        if(QApplication::keyboardModifiers() && Qt::ControlModifier && (keyEvent->key() == Qt::Key_Right))
        {
            on_btnNext_clicked();
            return QMainWindow::eventFilter(object, event);
        }
        /* keyboard shortcut for previous days*/
        if(QApplication::keyboardModifiers() && Qt::ControlModifier && (keyEvent->key() == Qt::Key_Left))
        {
            on_btnPrevious_clicked();
            return QMainWindow::eventFilter(object, event);
        }
        /* keyboard shortcut for today*/
        if(QApplication::keyboardModifiers() && Qt::ControlModifier && (keyEvent->key() == Qt::Key_Down))
        {
            on_btnToday_clicked();
            return QMainWindow::eventFilter(object, event);
        }
        if(!UNSAVED)  // detect keypress to notify user that new changes has been made
        {
            UNSAVED = true;
            this->setWindowTitle(this->windowTitle() + "*");  // add asterisk to title as sign that there is a change
        }
        if(QApplication::keyboardModifiers() && Qt::ControlModifier && (keyEvent->key() == Qt::Key_Equal))
        {
            ui->plainTextEdit->zoomIn(1);  // increase font size
        }
        if(QApplication::keyboardModifiers() && Qt::ControlModifier && (keyEvent->key() == Qt::Key_Minus))
        {
            ui->plainTextEdit->zoomOut(1);  // decrease font size
        }
    }
    return QMainWindow::eventFilter(object, event);
}
