#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnToday_clicked();

    void on_btnPrevious_clicked();

    void on_btnNext_clicked();

    void on_dateEdit_dateChanged(const QDate &date);

private:
    Ui::MainWindow *ui;
    static QString VERSION;
    static bool UNSAVED;
    static bool BUTTON_ISCLICKED;
    static const QString WATERMARK;
    void SaveNotes();
    bool eventFilter(QObject *object, QEvent *event);
};

#endif // MAINWINDOW_H
