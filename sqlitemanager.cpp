#include "sqlitemanager.h"
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QMessageBox>
#include <QCoreApplication>
#include "mainwindow.h"
#include <QFileInfo>

SqliteManager::SqliteManager()
{
    // check db connection during initialization
    const QString DRIVER("QSQLITE");

    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);

        // get current directory path
        QString currentPath = QCoreApplication::applicationDirPath();
        currentPath += "/journal.db";

        bool isDbExist = fileExists(currentPath);  // flag for file availability
        db.setDatabaseName(currentPath);

        if(!db.open())
        {
            messageBoxShow("Error opening the DB!");

            //qWarning() << "MainWindow::DatabaseConnect - ERROR: " << db.lastError().text();
        }

        // create table if db just newly created
        if(!isDbExist)
        {
            QSqlQuery querytable("CREATE TABLE `journal` (`date` TEXT, `content` TEXT, PRIMARY KEY(date))");
            if(!querytable.isActive())
            {
                messageBoxShow("Failed to create table in database!");
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Error! No Sqlite driver!");
        msgBox.exec();
        //qWarning() << "MainWindow::DatabaseConnect - ERROR: no driver " << DRIVER << " available";
    }
}

bool SqliteManager::fileExists(QString path) {
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    return check_file.exists() && check_file.isFile();
}

qint8 SqliteManager::Query(const QString &sql)
{
    qint8 retval = -1;

    QSqlQuery query;
    query.prepare(sql);
    if(!query.exec())
    {
        messageBoxShow("Failed to insert in database!\n" + query.lastError().text());
    }
    retval = query.numRowsAffected();
    return retval;
}

QSqlQuery SqliteManager::QueryScalar(const QString &sql)
{
    QSqlQuery query;
    query.prepare(sql);
    if(!query.exec())
        messageBoxShow(query.lastError().text());

    return query;
}

void SqliteManager::messageBoxShow(const QString &message)
{
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.exec();
}

