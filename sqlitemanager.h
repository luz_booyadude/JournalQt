#ifndef SQLITEMANAGER_H
#define SQLITEMANAGER_H

#include "mainwindow.h"
#include <QSqlQuery>

class SqliteManager
{
public:
    SqliteManager();
    static qint8 Query(const QString &sql);
    static QSqlQuery QueryScalar(const QString &sql);
    static void messageBoxShow(const QString &message);
private:
    bool fileExists(QString path);
    static QSqlDatabase db;
};

#endif // SQLITEMANAGER_H
